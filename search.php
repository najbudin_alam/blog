<?php include 'includes/db.php' ?>

<?php include 'includes/header.php' ?>
<!-- naviation -->
<?php include 'includes/nav.php' ?>

<!-- Page Content -->
<div class="container">
    <div class="row">
        <!-- Blog Entries Column -->
        <div class="col-md-8">

            <?php
            if (isset($_POST['submit'])) {

                $search = $_POST['search']; 
                $Query = "SELECT * FROM posts WHERE post_title OR post_tag LIKE '%$search%'";
                $sQuery = mysqli_query($connect, $Query);
                if (!$sQuery) {
                    die("Query Failed" . mysqli_error($connect));
                }

                $count = mysqli_num_rows($sQuery);

                if ($count === 0) {
                    echo "<h3> Sorry No result to display Please try again with different keywords </h3>";
                } else {

                    while ($row = mysqli_fetch_assoc($sQuery)) {
                        $post_title = $row['post_title'];
                        $post_author = $row['post_author'];
                        $post_date = $row['post_date'];
                        $post_image = $row['post_image'];
                        $post_content = $row['post_content'];
                        $post_tag = $row['post_tag'];
            ?>
                        <h1 class="page-header">
                            Page Heading
                            <small>Secondary Text</small>
                        </h1>
                        <!-- First Blog Post -->
                        <h2>
                            <a href="#"><?php echo $post_title ?></a>
                        </h2>
                        <p class="lead">
                            by <a href="index.php"><?php echo $post_author ?></a>
                        </p>

                        <p><span class="glyphicon glyphicon-time"></span> Posted on <?php echo $post_date ?></p>
                        <hr>
                        <img class="img-responsive" src="images/<?php echo $post_image; ?>" style="height: 350px; width: 100%; object-fit: cover;" alt="">
                        <hr>
                        <p><?php echo $post_content ?></p>
                        <a class="btn btn-primary" href="#">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>
                        <hr>

                        <!-- Pager -->
                        <ul class="pager">
                            <li class="previous">
                                <a href="#">&larr; Older</a>
                            </li>
                            <li class="next">
                                <a href="#">Newer &rarr;</a>
                            </li>
                        </ul>

            <?php }
                }
            } ?>

        </div>

        <!-- Blog Sidebar Widgets Column -->
        <?php include "includes/sidebar.php"; ?>

    </div>
    <!-- /.row -->

    <hr>

    <?php

    include "includes/footer.php";

    ?>