<?php ob_start(); ?>
<?php include "../includes/db.php"; ?>
<?php include "includes/header.php"; ?>

<body>
    <div id="wrapper">
        <!-- Navigation -->
        <?php include "includes/nav.php"; ?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <h2 class="page-header">
                     Welcome  Admin
                    </h2>
                    <?php
                    if (isset($_GET['source'])) {

                        $source = $_GET['source'];
                    } else {

                        $source = '';
                    }

                    switch ($source) {

                        case 'add_post';
                            include "includes/add_posts.php";
                            break;
                        case 'edit_post';
                            include "includes/edit_post.php";
                            break;

                        case '200';
                            echo "NICE 200";
                            break;

                        default:
                            include "includes/view_all_posts.php";
                            break;
                    }
                    ?>

                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->
    <?php include "includes/footer.php"; ?>

    <?php ob_end_flush(); ?>