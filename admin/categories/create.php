<?php include "./includes/db.php"; ?>
<?php include "../includes/header.php"; ?>

<body>

    <div id="wrapper">
        <!-- Navigation -->
    <?php include "../includes/nav.php"; ?>

        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">
                           Welcome To Admin Dashboard
                            <small>

                            <?php  if($connect) echo "Connected"; ?>
                            </small>
                        </h2>

                        <div class="col-xs-6">
                            <form action="">
                            <div class="form-group">
                                <label for="cat_title">Categroy Title</label>
                                <input type="text" name="cat_title" class="form-control" placeholder="Input Categroy Title">
                            </div>

                            <div class="form-grop">
                                <input type="submit" class="btn btn-sm btn-primary" value="Add Category">
                            </div>

                            </form>
                        </div>

                        <!-- <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i>
                            </li>
                        </ol> -->
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
 <?php include "../includes/footer.php"; ?>