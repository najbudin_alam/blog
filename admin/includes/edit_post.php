<?php //Select All data Query

if (isset($_GET['p_id'])) {
    $the_post_id = $_GET['p_id'];
}

$query = "SELECT * FROM posts Where post_id = $the_post_id";
$select_posts_by_id = mysqli_query($connect, $query);
while ($row = mysqli_fetch_assoc($select_posts_by_id)) {
    $posts_id = $row['post_id'];
    $posts_category_id = $row['post_category_id'];
    $posts_title = $row['post_title'];
    $posts_author = $row['post_author']; 
    $posts_date = $row['post_date'];
    $posts_image = $row['post_image'];
    $posts_content = $row['post_content'];
    $posts_tag = $row['post_tag'];
    $posts_status = $row['post_status'];
}



// Update Queries

if(isset($_POST['update_post'])) {
        
        
    // $post_user           =  escape($_POST['post_user']);
    $posts_title          =  escape($_POST['post_title']);
    $posts_category_id    =  escape($_POST['post_category_id']);
    $posts_status         =  escape($_POST['post_status']);
    $posts_image          =  escape($_FILES['post_image']['name']);
    $posts_image_tmp     =  escape($_FILES['post_image']['tmp_name']);
    $posts_content        =  escape($_POST['post_content']);
    $posts_tag           =  escape($_POST['post_tag']);
    
    move_uploaded_file($posts_image_tmp, "../images/$posts_image"); 
    
    if(empty($posts_image)) {
    
    $query = "SELECT * FROM posts WHERE post_id = $the_post_id ";
    $select_image = mysqli_query($connect,$query);
        
    while($row = mysqli_fetch_array($select_image)) {
        
       $posts_image = $row['post_image'];
    
    }
    
    
}
    $posts_title = mysqli_real_escape_string($connect, $posts_title);

    
      $query = "UPDATE posts SET ";
      $query .="post_title  = '{$posts_title}', ";
      $query .="post_category_id = '{$posts_category_id}', ";
      $query .="post_date   =  now(), ";
    //   $query .="post_user = '{$post_user}', ";
      $query .="post_status = '{$posts_status}', ";
      $query .="post_tag   = '{$posts_tag}', ";
      $query .="post_content= '{$posts_content}', ";
      $query .="post_image  = '{$posts_image}' ";
      $query .= "WHERE post_id = {$the_post_id} ";
    
    $update_post = mysqli_query($connect,$query);
    
    confirmQuery($update_post);
    
    echo "<p class='bg-success'>Post Updated. <a href='posts.php'>Edit More Posts</a></p>";
    
}

?>


<div class="card" style="padding: 20px;;">
    <form action="" method="POST" enctype="multipart/form-data">
        <div class="form-group">
            <label for="cat_title">Post Title</label>
            <input type="text" name="post_title" value="<?php echo $posts_title; ?>" class="form-control" placeholder="Input post Title">
        </div>
        <div class="form-group">
            <label for="cat_title">Post Category Id</label>
            <select name="post_category_id" class="form-control" id="post_category_id">
                <?php
                $query = "SELECT * FROM categories";
                $select_categories = mysqli_query($connect, $query);
                confirmQuery($select_categories);
                while ($row = mysqli_fetch_assoc($select_categories)) {
                    $cat_id = $row['cat_id'];
                    $cat_title = $row['cat_title'];
                    if ($cat_id == $posts_category_id) {
                        echo "<option selected value='{$cat_id}'>{$cat_title}</option>";
                    } else {
                        echo "<option value='{$cat_id}'>{$cat_title}</option>";
                    }
                }
                ?>
            </select>

        </div>
        <div class="form-group">
            <label for="cat_title">Post Author</label>
            <input type="text" name="post_author" value="<?php echo $posts_author; ?>" class="form-control" placeholder="Input post Author">
        </div>
        <div class="form-group">
            <label for="cat_title">Post Status</label>
            <input type="text" name="post_status" value="<?php echo $posts_status; ?>" class="form-control" placeholder="Input post Status">
        </div>
        <div class="form-group">
            <label for="cat_title">Post Content</label>
            <textarea type="text" name="post_content" class="form-control" rows="5" placeholder="Input post Content"> <?php echo $posts_content; ?> </textarea>
        </div>
        <div class="form-group">

            <img src="../images/<?php echo $posts_image ?>" width="100" class="mb-3" alt="">
            <?php echo "</br>"; ?>
            <?php echo "</br>"; ?>
            <label for="cat_title"> Add New Post Image</label>
            <input type="file" name="post_image" class="form-control">
        </div>
        <div class="form-group">
            <label for="cat_title">Post Tags</label>
            <input type="text" name="post_tag" value="<?php echo  $posts_tag; ?>" class="form-control" placeholder="Input post Tags">
        </div>
        <div class="form-group">
            <label for="cat_title">Post Date</label>
            <input type="date" name="post_date" value="<?php echo $posts_date; ?>" class="form-control" placeholder="Input post Date">
        </div>

        <div class="form-grop">
            <input type="submit" name="update_post" class="btn btn-sm btn-primary" value="Update Post">
        </div>

    </form>
</div>