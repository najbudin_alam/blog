
<?php
   

   if(isset($_POST['create_post'])) {
   
            $post_title        = escape($_POST['post_title']);
            $post_category_id  = escape($_POST['post_category_id']);
            $post_status       = escape($_POST['post_status']);
    
            $post_image        = escape($_FILES['post_image']['name']);
            $post_image_temp   = escape($_FILES['post_image']['tmp_name']);
    
    
            $post_tag         = escape($_POST['post_tag']);
            $post_content      = escape($_POST['post_content']);
            $post_date         = escape(date('d-m-y'));

       
        move_uploaded_file($post_image_temp, "../images/$post_image" );
       
       
      $query = "INSERT INTO posts(post_category_id, post_title, post_date,post_image,post_content,post_tag,post_status) ";
             
      $query .= "VALUES({$post_category_id},'{$post_title}',now(),'{$post_image}','{$post_content}','{$post_tag}', '{$post_status}') "; 
             
      $create_post_query = mysqli_query($connect, $query);  
          
      confirmQuery($create_post_query);

      $the_post_id = mysqli_insert_id($connect);

      echo "<p class='bg-success'>Post Created. <a href='posts.php'>view More Posts</a></p>";
       
   }
       
?>


<div class="card" style="padding: 20px;;">
    <form action="" method="POST" enctype="multipart/form-data">
        <div class="form-group">
            <label for="cat_title">Post Title</label>
            <input type="text" name="post_title" class="form-control" placeholder="Input post Title">
        </div>
        <div class="form-group">
            <label for="cat_title">Post Category Id</label>
            <input type="text" name="post_category_id" class="form-control" placeholder="Input Category ID">
        </div>
        <div class="form-group">
            <label for="cat_title">Post Author</label>
            <input type="text" name="post_author" class="form-control" placeholder="Input post Author">
        </div>
        <div class="form-group">
            <label for="cat_title">Post Status</label>
            <input type="text" name="post_status" class="form-control" placeholder="Input post Status">
        </div>
        <div class="form-group">
            <label for="cat_title">Post Content</label>
            <textarea type="text" name="post_content" class="form-control" rows="5" placeholder="Input post Content"> </textarea>
        </div>
        <div class="form-group">
            <label for="cat_title">Post Image</label>
            <input type="file" name="post_image" class="form-control">
        </div>
        <div class="form-group">
            <label for="cat_title">Post Tags</label>
            <input type="text" name="post_tag" class="form-control" placeholder="Input post Tags">
        </div>
        <div class="form-group">
            <label for="cat_title">Post Date</label>
            <input type="date" name="post_date" class="form-control" placeholder="Input post Date">
        </div>

        <div class="form-grop">
            <input type="submit" name="create_post" class="btn btn-sm btn-primary" value="Add Post">
        </div>

    </form>
</div>