<div class="col-xs-12">
    <table class="table text-center table-bordered table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>AUthor</th>
                <th>Title</th>
                <th>contents</th>
                <th>Status</th>
                <th>Image</th>
                <th>Tags</th>
                <th>Date</th>
                <th>Action</th>
            </tr>
        </thead>

        <tbody>
            <?php //Select All data Query
            $query = "SELECT * FROM posts";
            $select_all_posts = mysqli_query($connect, $query);
            while ($row = mysqli_fetch_assoc($select_all_posts)) {
                $posts_id = $row['post_id'];
                $posts_category_id = $row['post_category_id'];
                $posts_title = $row['post_title'];
                $posts_author = $row['post_author'];
                $posts_date = $row['post_date'];
                $posts_image = $row['post_image'];
                $posts_content = $row['post_content'];
                $posts_tag = $row['post_tag'];
                $posts_status = $row['post_status'];
                echo "<tr>";
                echo "<th>{$posts_id}</th>";
                echo  "<th>{$posts_author}</th>";
                echo  "<th>{$posts_title}</th>";
                echo  "<th>{$posts_content}</th>";
                echo  "<th>{$posts_status}</th>";
                echo  "<th> <img width='85' height = '75' src= '../images/{$posts_image}'></th>";
                echo  "<th>{$posts_tag}</th>";
                echo  "<th>{$posts_date}</th>";
                echo  "<th> <a href='posts.php?delete={$posts_id} '>Delete</a> </th>";
                echo  "<th> <a href='posts.php?source=edit_post&p_id={$posts_id} '>Edit</a> </th>";
                echo "</tr>";
            }
            ?>



            <?php //Delete Query
            if (isset($_GET['delete'])) {
                $post_id = $_GET['delete'];
                $query = "DELETE FROM posts WHERE post_id = {$post_id}" or
                    die("Could not Delete" . mysqli_error($connect));
                $delete_Query = mysqli_query($connect, $query);
                header("Location: posts.php");
            }
            ?>

        </tbody>
    </table>
</div>