<?php ob_start(); ?>
<?php include "../includes/db.php"; ?>
<?php include "includes/header.php"; ?>

<body>
    <div id="wrapper">
        <!-- Navigation -->
        <?php include "includes/nav.php"; ?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">
                            Create and List of Categories
                        </h2>

                        <div class="col-xs-6">
                            <?php

                            if (isset($_POST['submit'])) {
                                $cat_title = $_POST['cat_title'];


                                if ($cat_title == "" || empty($cat_title)) {
                                    echo "Category Title Can't Be Empty.";
                                } else {
                                    $query = "INSERT INTO categories(cat_title)";
                                    $query .= "VALUE('{$cat_title}')";

                                    $submit = mysqli_query($connect, $query);
                                }

                                if (!$submit) {
                                    die('Query Failed Something Went Wrong' . mysqli_error($connect));
                                }
                            }

                            ?>

                            <form action="" method="POST">
                                <div class="form-group">
                                    <label for="cat_title">Categroy Title</label>
                                    <input type="text" name="cat_title" class="form-control" placeholder="Input Categroy Title">
                                </div>

                                <div class="form-grop">
                                    <input type="submit" name="submit" class="btn btn-sm btn-primary" value="Add Category">
                                </div>

                            </form>

                            <?php

                            if (isset($_GET['edit'])) {
                                $cat_id = $_GET['edit'];
                                include "includes/update_category.php";
                            }
                            ?>

                        </div>

                        <div class="col-xs-6">
                            <table class="table text-center table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Category Title</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php //Select All data Query
                                    $query = "SELECT * FROM categories";
                                    $select_all_categories = mysqli_query($connect, $query);
                                    while ($row = mysqli_fetch_assoc($select_all_categories)) {
                                        $cat_id = $row['cat_id'];
                                        $cat_title = $row['cat_title'];
                                        echo "<tr>";
                                        echo "<th>{$cat_id}</th>";
                                        echo  "<th>{$cat_title}</th>";
                                        echo  "<th> <a href='categories.php?delete={$cat_id} '>Delete</a> </th>";
                                        echo  "<th> <a href='categories.php?edit={$cat_id} '>Edit</a> </th>";
                                        echo "</tr>";
                                    }
                                    ?> 

                                    <?php //Delete Query
                                    if (isset($_GET['delete'])) {
                                        $cat_id = $_GET['delete'];
                                        $query = "DELETE FROM categories WHERE cat_id= {$cat_id}" or
                                            die("Could not Delete" . mysqli_error($connect));
                                        $delete_Query = mysqli_query($connect, $query);
                                        header("Location: categories.php");
                                    }
                                    ?>

                                </tbody>
                            </table>
                        </div>

                        <!-- <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i>
                            </li>
                        </ol> -->
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
        <?php include "includes/footer.php"; ?>

        <?php ob_end_flush(); ?>